'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Customer = require('./domain');

const postOneCustomer = async (payload) => {
    const customer = new Customer();
    const postCommand = async (payload) => {
        return await customer.addNewCustomer(payload);
    }
    return postCommand(payload);
}

const deleteOneCustomer = async (id) => {
    const customer = new Customer();
    const delCommand = async (id) => {
        return await customer.deleteCustomer(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneCustomer : postOneCustomer,
    deleteOneCustomer : deleteOneCustomer
}