'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneCustomer = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('customer');
    const result = await db.insertOne(document);
    return result;
}

const updateOneCustomer = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('customer');
    const result = await db.upsertOne(params, document);
    return result;
}

const deleteOneCustomer = async (params) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('customer');
    const result = await db.deleteOne(params);
    return result;
}

module.exports = {
    insertOneCustomer: insertOneCustomer,
    updateOneCustomer: updateOneCustomer,
    deleteOneCustomer: deleteOneCustomer
}