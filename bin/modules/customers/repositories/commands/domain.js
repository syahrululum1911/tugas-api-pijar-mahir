'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Customer{

    async addNewCustomer(payload){
        const data = [payload];
        let view = model.generalCustomer();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.customer_name)){accumulator.customer_name = value.customer_name;}     
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneCustomer(document);
        return result;
    }

    async updateCustomer(params, payload){
        const data = [payload];
        let view = model.generalCustomer();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.customer_name)){accumulator.customer_name = value.customer_name;}       
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneCustomer(params, document);
        return result;
    }

    async deleteCustomer(params){
        const result = await command.deleteOneCustomer(params);
        return result;
    }

}

module.exports = Customer;