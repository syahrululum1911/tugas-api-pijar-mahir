'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Customer = require('./domain');

const getOneCustomer = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const customer = new Customer(queryParam);
        const result = await customer.viewOneCustomer();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllCustomers = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const customer = new Customer(queryParam);
        const result = await customer.viewAllCustomers();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneCustomer : getOneCustomer,
    getAllCustomers : getAllCustomers
}