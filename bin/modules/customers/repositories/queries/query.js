'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneCustomer = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('customer');
    const recordset = await db.findOne(parameter);
    return recordset;
}

const findAllCustomers = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('customer');
    const recordset = await db.findMany();
    return recordset;
}


module.exports = {
    findOneCustomer: findOneCustomer,
    findAllCustomers: findAllCustomers
}