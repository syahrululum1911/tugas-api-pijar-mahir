'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Customer{
    constructor(param){
        this.id = param.id,
        this.customer_name = param.customer_name;
        this.createdAt =  param.createdAt,
        this.updatedAt = param.updatedAt
    }

    async viewOneCustomer(){
        const param = {"id": this.id};
        const result = await query.findOneCustomer(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }


    async viewAllCustomers(){
        const param = {};
        const result = await query.findAllCustomers(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

}

module.exports = Customer;