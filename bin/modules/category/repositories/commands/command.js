'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneCategory = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('category');
    const result = await db.insertOne(document);
    return result;
}

const updateOneCategory = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('category');
    const result = await db.upsertOne(params, document);
    return result;
}

const deleteOneCategory = async (params) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('category');
    const result = await db.deleteOne(params);
    return result;
}

module.exports = {
    insertOneCategory: insertOneCategory,
    updateOneCategory: updateOneCategory,
    deleteOneCategory: deleteOneCategory
}