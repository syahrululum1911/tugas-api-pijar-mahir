'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneCourse = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.course_name] = {length: {minimum: 5}};
    values[payload.course_name] = payload.course_name;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllCourses = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.course_name] = {length: {minimum: 5}};
    values[payload.course_name] = payload.course_name;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneCourse = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.course_name] = {length: {minimum: 5}};
    values[payload.course_name] = payload.course_name;
    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParamGetOneCourse: isValidParamGetOneCourse,
    isValidParamGetAllCourses: isValidParamGetAllCourses,
    isValidParamPostOneCourse: isValidParamPostOneCourse
}